const batu_player = document.getElementById('batu-player')
const kertas_player = document.getElementById('kertas-player')
const gunting_player = document.getElementById('gunting-player')
const batu_com = document.getElementById('batu-com')
const kertas_com = document.getElementById('kertas-com')
const gunting_com = document.getElementById('gunting-com')
const btn_refresh = document.getElementById('btn-refresh')
const hasil_janken = document.getElementById('hasil')

function click_batu(){
    // let image = new Image(200,200)
    // image.src = "url('assets/img/janken/rectangle_biru.jpg')"

    batu_player.style.backgroundImage = "url('assets/img/janken/rectangle_biru.jpg')"
    batu_player.style.backgroundPosition = '200px 200px'
    batu_player.style.backgroundPosition = 'center'
    random()
}

function click_kertas(){
    kertas_player.style.backgroundImage = "url('assets/img/janken/rectangle_biru.jpg')"
    kertas_player.style.backgroundSize = '200px 200px'
    kertas_player.style.backgroundPosition = 'center'
    random()
}

function click_gunting(){
    gunting_player.style.backgroundImage = "url('assets/img/janken/rectangle_biru.jpg')"
    gunting_player.style.backgroundSize = '200px 200px'
    gunting_player.style.backgroundPosition = 'center'
    random()
}


batu_player.addEventListener('click', click_batu)
kertas_player.addEventListener('click', click_kertas)
gunting_player.addEventListener('click', click_gunting)
btn_refresh.addEventListener('click', refresh)

function random(){
    let min = 1
    let max = 3

    const number_random = Math.floor(Math.random() * (max - min + 1) + min)
    
    switch (number_random) {
        case 1 :
            batu_com.style.backgroundImage = "url('assets/img/janken/rectangle_biru.jpg')"
            batu_com.style.backgroundSize = '200px 200px'
            batu_com.style.backgroundPosition = 'center'
            console.log('batu')
            console.log(number_random)
            break
        case 2 :
            kertas_com.style.backgroundImage = "url('assets/img/janken/rectangle_biru.jpg')"
            kertas_com.style.backgroundSize = '200px 200px'
            kertas_com.style.backgroundPosition = 'center'
            console.log('kertas')
            console.log(number_random)
            break
        case 3 :
            gunting_com.style.backgroundImage = "url('assets/img/janken/rectangle_biru.jpg')"
            gunting_com.style.backgroundSize = '200px 200px'
            gunting_com.style.backgroundPosition = 'center'
            console.log('gunting')
            console.log(number_random)
            break
    }
}

function refresh() {
    batu_player.style.backgroundImage = 'none'
    batu_com.style.backgroundImage = 'none'
    kertas_player.style.backgroundImage = 'none'
    kertas_com.style.backgroundImage = 'none'
    gunting_player.style.backgroundImage = 'none'
    gunting_com.style.backgroundImage = 'none'
    hasil_janken.innerHTML = "VS"
}

function janken_batu() {
    if (click_batu && number_random == 1) {
        hasil_janken.innerText = 'DRAW'
        console.log('janken_batu-batu') 
        console.log(random().number_random)              
    } else if (click_kertas && number_random == 1) {
        hasil_janken.innerText = 'PLAYER 1 WIN'
        console.log('janken_batu-kertas')
        console.log(random()) 
    } else if (click_gunting && number_random == 1){
        hasil_janken.innerText = 'COM WIN'
        console.log('janken_batu-gunting')
        console.log(random()) 
    }
} 

function janken_kertas() {
    if (click_batu && number_random == 2) {
        hasil_janken.innerText = 'COM WIN'
        console.log('janken_kertas-batu')               
    } else if (click_kertas && number_random == 2) {
        hasil_janken.innerText = 'DRAW' 
        console.log('janken_kertas-kertas')
    } else if (click_gunting && number_random == 2){
        hasil_janken.innerText = 'PLAYER 1 WIN'
        console.log('janken_kertas-gunting') 
    }
}

function janken_gunting() {
    if (click_batu && number_random == 3) {
        hasil_janken.innerText = 'PLAYER 1 WIN' 
        console.log('janken_gunting-batu')              
    } else if (click_kertas && number_random == 3) {
        hasil_janken.innerText = 'COM WIN'
        console.log('janken_gunting-kertas') 
    } else if (click_gunting && number_random == 3){
        hasil_janken.innerText = 'DRAW'
        console.log('janken_gunting-gunting') 
    }
}


